package com.sh1rogane.ld31;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.util.List;

import com.sh1rogane.ld31.entitys.Entity;

public class GameRenderer extends Canvas
{
	private static final long serialVersionUID = 1L;

	private int renderWidth;
	private int renderHeight;
	private BufferStrategy bs;
	
	public int offsetX;
	public int offsetY;
	
	public GameRenderer(int renderWidth, int renderHeight)
	{
		this.renderWidth = renderWidth;
		this.renderHeight = renderHeight;
		
	}
	public void init()
	{
		this.setPreferredSize(new Dimension(renderWidth, renderHeight));
		this.setIgnoreRepaint(true);
		this.createBufferStrategy(2);
		bs = this.getBufferStrategy();
	}
	public void setOffset(int offsetX, int offsetY)
	{
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	public void render(List<Entity> gameObjects)
	{
		Graphics2D g = null;

		try
		{
			g = (Graphics2D) bs.getDrawGraphics();
			
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			
			AffineTransform at = g.getTransform();
			
			g.clearRect(0, 0, renderWidth, renderHeight);
			g.setColor(Color.WHITE);
			
			g.translate(-offsetX, -offsetY);
			g.clearRect(0, 0, renderWidth, renderHeight);
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, renderWidth, renderHeight);

			for(Entity e : gameObjects)
			{
				if(e.isInsideRenderView())
				{
					e.render(g);
				}
			}
			g.setTransform(at);
		}
		catch(IllegalStateException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(g != null)
				g.dispose();
		}
		if(bs != null)
			bs.show();

		//Toolkit.getDefaultToolkit().sync();
	}
}
