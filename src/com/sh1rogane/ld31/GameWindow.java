package com.sh1rogane.ld31;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;

public class GameWindow extends JFrame implements ComponentListener
{
	private static final long serialVersionUID = 1L;
	
	private GameThread gt;
	private GameRenderer gr;
	private int width;
	private int height;
	
	private int x;
	private int y;
	
	public GameWindow(int width, int height, GameThread gt, GameRenderer gr)
	{
		this.gt = gt;
		this.gr = gr;
		this.width = width;
		this.height = height;
		
		initFrame();
	}
	private void initFrame()
	{
		//this.setLayout(null);
		this.setIgnoreRepaint(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		this.setResizable(false);
		
		this.add(gr);
		this.setVisible(true);
		gr.init();

		this.pack();

		this.setSize(width, height);
		this.setLocationRelativeTo(null);
		this.x = this.getX();
		this.y = this.getY();
		
		this.requestFocus();
		gr.requestFocusInWindow();
		
		gr.addKeyListener(Input.getInstance());
		
		this.addComponentListener(this);
	}
	@Override
	public void componentHidden(ComponentEvent e)
	{
		
	}
	@Override
	public void componentMoved(ComponentEvent e)
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int sw = screenSize.width;
		int sh = screenSize.height;
		
		int minX = sw / 2 - 640;
		int maxX = sw / 2 + 640 - width;
		int minY = sh / 2 - 360;
		int maxY = sh / 2 + 360 - height;
		
//		if(getX() <= minX)
//		{
//			this.setLocation(minX, getY());
//		}
//		else if(getX() >= maxX + 10)
//		{
//			this.setLocation(maxX + 10, getY());
//		}
//		if(getY() <= minY - 20)
//		{
//			this.setLocation(getX(), minY - 20);
//		}
//		else if(getY() >= maxY + 10)
//		{
//			this.setLocation(getX(), maxY + 10);
//		}
		gr.setOffset(640 - width / 2 - (x - this.getX()), 360 - width / 2 - (y - this.getY()));
		Input.getInstance().clear();
	}
	@Override
	public void componentResized(ComponentEvent e)
	{
	}
	@Override
	public void componentShown(ComponentEvent e)
	{
	}

}
