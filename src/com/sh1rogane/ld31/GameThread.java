package com.sh1rogane.ld31;

public class GameThread extends Thread
{
	private int TICKS_PER_SECOND = 60;
	private double SKIP_TICKS = 1000.0 / TICKS_PER_SECOND;
	private final int MAX_FRAMESKIP = 5;

	private final int MAX_FRAME_RATE = 500;
	private final int FRAME_CAP = 1000 / MAX_FRAME_RATE;
	
	private double next_game_tick = 0;
	private int loops = 0;
	private double delta = 0;
	
	private boolean running;
	
	private GameThreadListener gtl;

	public GameThread(int tickRate)
	{
		TICKS_PER_SECOND = tickRate;
		SKIP_TICKS = 1000.0 / TICKS_PER_SECOND;
	}
	
	public void addGameThreadListener(GameThreadListener gtl)
	{
		this.gtl = gtl;
	}
	
	@Override
	public void start()
	{
		running = true;
		super.start();
	}
	public void forceRender()
	{
		if(gtl != null)
		{
			synchronized(gtl)
			{
				gtl.render();
			}
		}	
	}
	
	@Override
	public void run()
	{
		int frames = 0;
		int ticks = 0;
		long lastTimer1 = System.currentTimeMillis();
		next_game_tick = System.currentTimeMillis();
		while(running)
		{
			try
			{
				Thread.sleep(FRAME_CAP);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
			loops = 0;
			while(System.currentTimeMillis() > next_game_tick && loops < MAX_FRAMESKIP)
			{
				if(gtl != null)
				{
					gtl.tick();
				}
				next_game_tick += SKIP_TICKS;
				loops++;
				ticks++;
			}
			delta = (System.currentTimeMillis() + SKIP_TICKS - next_game_tick) / SKIP_TICKS;
			if(gtl != null)
			{
				gtl.render();
			}

			frames++;
			if(System.currentTimeMillis() - lastTimer1 > 1000)
			{
				lastTimer1 += 1000;
				if(gtl != null)
				{
					gtl.fpsUpdate(frames, ticks);
				}
				frames = 0;
				ticks = 0;
			}
		}
	}
}
