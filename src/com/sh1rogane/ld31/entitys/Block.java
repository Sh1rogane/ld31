package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Block extends Entity
{
	public Block(double x, double y, int width, int height)
	{
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		type = "block";
	}
	
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		g.setTransform(at);
	}
}
