package com.sh1rogane.ld31.entitys;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import com.sh1rogane.ld31.LD31;

public class Entity
{
	protected double x;
	protected double y;
	protected int width;
	protected int height;
	
	public String type = "";
	
	public void render(Graphics2D g)
	{
		
	}
	public void update()
	{
		
	}
	
	public boolean intersects(Entity other)
	{
		if(x + width > other.x && x < other.x + other.width && y + height > other.y && y < other.y + other.height)
		{
			return true;
		}
		return false;
	}
	
	public boolean isInsideView()
	{
		int ox = LD31.renderer.offsetX;
		int oy = LD31.renderer.offsetY;
		
		if(this.x < ox - width || this.x > ox + 250 || this.y < oy - height|| this.y > oy + 250 - height)
		{
			return false;
		}
		
		return true;
	}
	public boolean isInsideRenderView()
	{
		int ox = LD31.renderer.offsetX;
		int oy = LD31.renderer.offsetY;
		
		double minX1 = x - 100;
		double maxX1 = x + width + 100;
		double minX2 = ox;
		double maxX2 = ox + 250;
		double minY1 = y - 100;
		double maxY1 = y + height + 100;
		double minY2 = oy;
		double maxY2 = oy + 250;
		if(maxX1 < minX2 || maxY1 < minY2 || minX1 > maxX2 || minY1 > maxY2)
		{
			return false;
		}
		return true;
	}
	public Rectangle getHitbox()
	{
		return new Rectangle((int)x, (int) y, width, height);
	}
	public Entity collides(String type, double xSpeed, double ySpeed)
	{
		ArrayList<Entity> l = LD31.entitys;
		for(Entity e : l)
		{
			if(e.type.equals(type))
			{
				Rectangle r = e.getHitbox();
				if(getHitbox().intersects(r.x - xSpeed, r.y - ySpeed, r.width, r.height))
					return e;
			}
		}
		return null;
	}
	
	public void removeSelf()
	{
		LD31.removeEntitys.add(this);
	}
}
