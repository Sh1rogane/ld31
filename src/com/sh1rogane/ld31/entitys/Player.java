package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;

import com.sh1rogane.ld31.Input;
import com.sh1rogane.ld31.LD31;
import com.sh1rogane.ld31.Sound;

public class Player extends Entity
{
	private double vy = 0;
	private double vx = 0;
	private boolean inAir;
	
	public Player(double x, double y)
	{
		this.x = x;
		this.y = y;
		this.width = 20;
		this.height = 20;
		
		this.type = "player";
	}
	
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, width, height);
		g.setTransform(at);
	}
	public void update()
	{
		vy += 0.3;
		if(vy > 5)
		{
			vy = 5;
		}
		
		if(Input.isKeyDown(KeyEvent.VK_LEFT) || Input.isKeyDown(KeyEvent.VK_A))
		{
			vx = -5;
		}
		else if(Input.isKeyDown(KeyEvent.VK_RIGHT) || Input.isKeyDown(KeyEvent.VK_D))
		{
			vx = 5;
		}
		else
		{
			vx = 0;
		}
		if((Input.isKeyTyped(KeyEvent.VK_SPACE) || Input.isKeyDown(KeyEvent.VK_UP) || Input.isKeyDown(KeyEvent.VK_W)) && !inAir)
		{
			inAir = true;
			vy = -6;
			Sound.jump.play();
		}
		
		checkCollisions();
		
		if(this.collides("deadly", vx, vy) != null)
		{
			restart();
		}
		if(this.collides("win", vx, vy) != null)
		{
			win();
		}
		this.x += vx;
		this.y += vy;
	}
	private void restart()
	{
		Sound.hit.play();
		this.x = 550;
		this.y = 350;
		
		LD31.window.setLocationRelativeTo(null);
		
		LD31.addEntitys.add(new ShowText(550, 350, "You died, try again", 180));
	}
	private void win()
	{
		this.x = 550;
		this.y = 350;
		
		LD31.window.setLocationRelativeTo(null);
		
		LD31.addEntitys.add(new ShowText(575, 350, "YOU WON!", 600));
	}
	private void checkCollisions()
	{
		Entity e = collides("block", 0, vy);
		if(e != null)
		{
			if(vy > 0)
			{
				if(vy > 1)
				{
					Sound.hurt.play();
				}
				
				inAir = false;
				vy = 0;
				y = e.getHitbox().y - height;
				
				
			}
			else if(vy < 0)
			{
				vy = 0;
				y = e.getHitbox().getMaxY();
			}
		}
		e = collides("block", vx, 0);
		if(e != null)
		{
			if(vx > 0)
			{
				vx = 0;
				x = e.getHitbox().x - width;
			}
			else if(vx < 0)
			{
				vx = 0;
				x = e.getHitbox().getMaxX();
			}
		}
	}
}
