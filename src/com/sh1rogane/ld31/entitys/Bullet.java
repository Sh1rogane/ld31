package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Bullet extends Entity
{
	private double vx;
	private double vy;
	
	public Bullet(double x, double y, double vx, double vy)
	{
		this.x = x;
		this.y = y;
		this.width = 10;
		this.height = 10;
		
		this.vy = vy;
		this.vx = vx;
		
		this.type = "deadly";
	}
	public void update()
	{
		this.x += vx;
		this.y += vy;
		
		if(this.collides("block", vx, vy) != null)
		{
			removeSelf();
		}
	}
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setColor(Color.RED);
		g.fillRect(0, 0, width, height);
		g.setTransform(at);
	}
}
