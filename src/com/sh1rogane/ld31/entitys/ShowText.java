package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class ShowText extends Entity
{
	private String text;
	private Font font;
	
	private int duration;
	private int durationTime;
	
	public ShowText(double x, double y, String text, int duration)
	{
		this.x = x;
		this.y = y;
		this.text = text;
		font = new Font("Arial", Font.BOLD, 16);
		
		this.width = text.length() * 10;
		
		this.duration = duration;
	}
	
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setFont(font);
		g.setColor(Color.BLACK);
		g.drawString(text, 0, 0);
		g.setTransform(at);
	}
	public void update()
	{
		durationTime++;
		if(durationTime >= duration)
		{
			removeSelf();
		}
	}
}
