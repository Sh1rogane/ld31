package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import com.sh1rogane.ld31.LD31;

public class Shooter extends Entity
{
	private double shootTimer;
	private boolean up;
	
	public Shooter(double x, double y, boolean up)
	{
		this.x = x;
		this.y = y;
		this.width = 40;
		this.height = 20;
		this.up = up;
		
		this.type = "deadly";
	}
	
	public void update()
	{
		shootTimer++;
		if(shootTimer > 10)
		{
			shootTimer = 0;
			if(up)
			{
				LD31.addEntitys.add(new Bullet(x + 15, y - 10, 0, -5));
			}
			else
			{
				LD31.addEntitys.add(new Bullet(x + 15, y + 20, 0, 5));
			}
			
		}
	}
	
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setColor(Color.DARK_GRAY);
		
		g.fillRect(0, 0, width, height);
		if(up)
		{
			g.fillRect(10, -10, width / 2, height / 2);
		}
		else
		{
			g.fillRect(10, 20, width / 2, height / 2);
		}
		g.setTransform(at);
	}
	
}
