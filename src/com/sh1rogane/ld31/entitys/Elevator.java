package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Elevator extends Entity
{
	private boolean xAxis;
	private boolean moveTo = true;
	private boolean negativeDir;
	
	private double from;
	private double to;
	
	private double vx;
	private double vy;
	public Elevator(double x, double y, boolean xAxis, double from, double to)
	{
		this.x = x;
		this.y = y;
		this.width = 60;
		this.height = 20;
		
		this.xAxis = xAxis;
		this.from = from;
		this.to = to;
		
		if(to - from < 0)
		{
			negativeDir = true;
		}
		
		this.type = "block";
	}
	public void update()
	{
		if(xAxis)
		{
			if(moveTo)
			{
				if(negativeDir)
				{
					this.vx = -1;
					if(this.x <= to)
					{
						moveTo = false;
					}
				}
				else
				{
					this.vx = 1;
					if(this.x >= to)
					{
						moveTo = false;
					}
				}
			}
			else
			{
				if(negativeDir)
				{
					this.vx = 1;
					if(this.x >= from)
					{
						moveTo = true;
					}
				}
				else
				{
					this.vx = -1;
					if(this.x <= from)
					{
						moveTo = true;
					}
				}
			}
		}
		else
		{
			if(moveTo)
			{
				if(negativeDir)
				{
					this.vy = -1;
					if(this.y <= to)
					{
						
						moveTo = false;
					}
				}
				else
				{
					this.vy = 1;
					if(this.y >= to)
					{
						moveTo = false;
					}
				}
			}
			else
			{
				if(negativeDir)
				{
					this.vy = 1;
					if(this.y >= from)
					{
						moveTo = true;
					}
				}
				else
				{
					this.vy = -1;
					if(this.y <= from)
					{
						moveTo = true;
					}
				}
			}
		}
		this.x += vx;
		this.y += vy;
		
		Entity p = collides("player", 0, -5);
		if(p != null)
		{
			p.x += vx;
			p.y += vy;
		}
	}
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		
		g.translate(x, y);
		g.setColor(Color.gray);
		g.fillRect(0, 0, width, height);
		g.setTransform(at);
	}
}
