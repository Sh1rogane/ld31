package com.sh1rogane.ld31.entitys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class Laser extends Entity
{
	private int delay;
	private int delayTime = 0;
	private boolean delayDone;
	
	private int stopTime;
	
	private double end;
	private double start;
	private boolean toEnd = true;
	
	private int dir;
	
	public Laser(double x, double y, int width, int height, int dir, int delay)
	{
		this.x = x;
		this.y = y;
		
		this.delay = delay;
		this.dir = dir;
		
		this.width = width;
		this.height = height;
		//up
		if(dir == 0)
		{
			start = y;
			end = y - height;
		}
		//right
		if(dir == 1)
		{
			start = x;
			end = x + width;
		}
		//down
		if(dir == 2)
		{
			start = y;
			end = y + height;
		}
		//left
		if(dir == 3)
		{
			start = x;
			end = x - width;
		}
		
		this.type = "deadly";
	}
	
	public void update()
	{
		delayTime++;
		if(!delayDone && delayTime >= delay)
		{
			delayDone = true;
		}
		if(delayDone)
		{
			//up
			if(dir == 0)
			{
				if(toEnd)
				{
					y -= 3;
					if(y <= end)
					{
						toEnd = false;
					}
				}
				else
				{
					y += 3;
					if(y >= start)
					{
						toEnd = true;
					}
				}
			}
			//right
			if(dir == 1)
			{
				if(toEnd)
				{
					x += 3;
					if(x >= end)
					{
						toEnd = false;
					}
				}
				else
				{
					x -= 3;
					if(x <= start)
					{
						toEnd = true;
					}
				}
			}
			//down
			if(dir == 2)
			{
				if(toEnd)
				{
					y += 3;
					if(y >= end)
					{
						toEnd = false;
					}
				}
				else
				{
					y -= 3;
					if(y <= start)
					{
						toEnd = true;
					}
				}
			}
			//left
			if(dir == 3)
			{
				if(toEnd)
				{
					x -= 3;
					if(x <= end)
					{
						toEnd = false;
					}
				}
				else
				{
					x += 3;
					if(x >= start)
					{
						toEnd = true;
					}
				}
			}
		}
		
	}
	public void render(Graphics2D g)
	{
		AffineTransform at = g.getTransform();
		g.translate(x, y);
		g.setColor(Color.RED);
		g.fillRect(0, 0, width, height);
		g.setTransform(at);
	}
}
