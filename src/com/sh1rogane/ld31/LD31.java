package com.sh1rogane.ld31;

import java.util.ArrayList;

import com.sh1rogane.ld31.entitys.Block;
import com.sh1rogane.ld31.entitys.Elevator;
import com.sh1rogane.ld31.entitys.Entity;
import com.sh1rogane.ld31.entitys.Laser;
import com.sh1rogane.ld31.entitys.Player;
import com.sh1rogane.ld31.entitys.Shooter;
import com.sh1rogane.ld31.entitys.Text;
import com.sh1rogane.ld31.entitys.WinBlock;


public class LD31 implements GameThreadListener
{
	private GameThread gt = new GameThread(60);
	public static GameRenderer renderer;
	public static GameWindow window;
	
	public static ArrayList<Entity> entitys = new ArrayList<Entity>();
	public static ArrayList<Entity> removeEntitys = new ArrayList<Entity>();
	public static ArrayList<Entity> addEntitys = new ArrayList<Entity>();
	
	private Player player = new Player(550, 720 / 2 - 10);
	
	private Text text = new Text(550, 300, "Try moving the window");
	private Text text2 = new Text(500, 325, "Objects only updates when in view");
	private Text text3 = new Text(600, 640, "GOAL");
	
	public LD31()
	{
		renderer = new GameRenderer(1280, 720);
		initWindow();
		startThread();
		createLevel();
	}
	private void createLevel()
	{
		addEntitys.add(text);
		addEntitys.add(text2);
		addEntitys.add(text3);
		
		//Lasers
		addEntitys.add(new Laser(200, 400, 100, 20, 1, 0));
		addEntitys.add(new Laser(200, 500, 100, 20, 1, 60));
		addEntitys.add(new Laser(200, 600, 100, 20, 1, 0));
		
		//Lasers
		addEntitys.add(new Laser(320, 500, 20, 100, 2, 0));
		addEntitys.add(new Laser(400, 500, 20, 100, 2, 60));
		addEntitys.add(new Laser(480, 500, 20, 100, 2, 0));
		
		//Outer walls
		addEntitys.add(new Block(0, 0, 1280, 20));
		addEntitys.add(new Block(0, 0, 20, 720));
		addEntitys.add(new Block(1260, 0, 20, 720));
		addEntitys.add(new Block(0, 700, 1280, 20));
		
		//End
		addEntitys.add(new Block(100, 100, 100, 20));
		addEntitys.add(new Block(0, 200, 150, 20));
		addEntitys.add(new Block(0, 400, 200, 20));
		addEntitys.add(new Block(300, 300, 100, 300));
		addEntitys.add(new Block(200, 400, 20, 300));
		addEntitys.add(new Block(200, 680, 300, 20));
		addEntitys.add(new Block(300, 500, 200, 120));
		
		addEntitys.add(new WinBlock(600, 650, 50, 50));

		//Start
		addEntitys.add(new Block(200, 100, 20, 180));
		addEntitys.add(new Block(100, 280, 320, 20));
		addEntitys.add(new Block(400, 300, 20, 100));
		addEntitys.add(new Block(400, 400, 300, 20));
		addEntitys.add(new Block(700, 400, 20, 300));
		addEntitys.add(new Block(800, 200, 20, 425));
		
		//First puzzle
		addEntitys.add(new Block(900, 660, 40, 40));
		addEntitys.add(new Block(940, 620, 40, 80));
		addEntitys.add(new Block(1100, 660, 40, 40));
		addEntitys.add(new Block(1060, 620, 40, 80));
		addEntitys.add(new Shooter(1000, 680, true));
		
		//Second puzzle
		addEntitys.add(new Block(1180, 620, 40, 80));
		addEntitys.add(new Block(1220, 580, 40, 120));
		addEntitys.add(new Block(900, 525, 275, 20));
		addEntitys.add(new Block(1000, 425, 175, 20));
		addEntitys.add(new Shooter(1000, 440, false));
		addEntitys.add(new Shooter(1135, 440, false));
		
		//Elevator
		addEntitys.add(new Elevator(825, 525, false, 525, 200));
		
		//
		addEntitys.add(new Block(900, 200, 375, 20));
		addEntitys.add(new Block(1180, 160, 40, 40));
		addEntitys.add(new Block(1220, 120, 40, 80));
		addEntitys.add(new Block(900, 80, 250, 20));
		
		//Second elevator
		addEntitys.add(new Elevator(200, 80, true, 200, 825));
		
		addEntitys.add(player);
	}
	private void initWindow()
	{
		window = new GameWindow(250, 250, gt, renderer);
		window.setTitle("LD31");
	}
	private void startThread()
	{
		gt.addGameThreadListener(this);
		gt.start();
	}
	@Override
	public void tick()
	{
		if(removeEntitys.size() > 0)
		{
			entitys.removeAll(removeEntitys);
			removeEntitys.clear();
		}
		if(addEntitys.size() > 0)
		{
			entitys.addAll(addEntitys);
			addEntitys.clear();
		}
		for(Entity e : entitys)
		{
			if(e.isInsideView())
			{
				e.update();
			}
			
		}
		Input.getInstance().tick();
	}

	@Override
	public void render()
	{
		renderer.render(entitys);
	}

	@Override
	public void fpsUpdate(int fps, int tps)
	{
		//window.setTitle("tick: " + tps + " fps: " + fps);
	}
	
	public static void main(String args[])
	{
		new LD31();
	}
}
