package com.sh1rogane.ld31;

public interface GameThreadListener
{
	public void tick();
	public void render();
	public void fpsUpdate(int fps, int tps);
}
