package com.sh1rogane.ld31;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound
{
	public static final Sound jump = new Sound("/Jump.wav", false);
	public static final Sound hurt = new Sound("/Hurt.wav", false);
	public static final Sound hit = new Sound("/Hit.wav", false);
	
	private AudioClip clip;
	private boolean loop;

	public Sound(String name, boolean loop)
	{
		try
		{
			this.loop = loop;
			clip = Applet.newAudioClip(Sound.class.getResource(name));
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
	}

	public void play()
	{
		try
		{
			new Thread()
			{
				@Override
				public void run()
				{
					if(loop)
						clip.loop();
					else
						clip.play();
				}
			}.start();
		}
		catch(Throwable e)
		{
		}
	}
	public void stop()
	{
		clip.stop();
	}
}